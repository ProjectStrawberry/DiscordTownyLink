package com.gmail.jd4656.discordtownylink;

import com.palmergames.bukkit.towny.TownyAPI;
import com.palmergames.bukkit.towny.event.*;
import com.palmergames.bukkit.towny.exceptions.NotRegisteredException;
import com.palmergames.bukkit.towny.object.Resident;
import com.palmergames.bukkit.towny.object.Town;
import github.scarsz.discordsrv.DiscordSRV;
import github.scarsz.discordsrv.dependencies.jda.api.entities.Member;
import github.scarsz.discordsrv.dependencies.jda.api.entities.TextChannel;
import github.scarsz.discordsrv.objects.managers.AccountLinkManager;
import github.scarsz.discordsrv.util.*;
import mineverse.Aust1n46.chat.api.events.VentureChatEvent;
import mineverse.Aust1n46.chat.channel.ChatChannel;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.List;


public class EventListeners implements Listener {

    Main plugin;

    EventListeners(Main p) {
        plugin = p;
    }

    @EventHandler
    public void onVentureChat(VentureChatEvent event) {
        Player player = event.getMineverseChatPlayer().getPlayer();
        ChatChannel channel = event.getChannel();

        if (channel.getName().equals("Town")) {
            try {
                Resident resident = TownyAPI.getInstance().getDataSource().getResident(player.getName());
                Town town = resident.getTown();

                TextChannel discordChannel = plugin.getChannel(town);
                if (discordChannel == null) return;

                String message = event.getChat();
                message = DiscordUtil.convertMentionsFromNames(message, DiscordSRV.getPlugin().getMainGuild());
                message = DiscordUtil.strip(message);
                WebhookUtil.deliverMessage(discordChannel, player, message);
                //WebhookUtil.deliverMessage(discordChannel, DiscordUtil.strip(player.getDisplayName()), DiscordSRV.getPlugin().getEmbedAvatarUrl(player), message, null);
            } catch (NotRegisteredException ignored) {}
        }
    }

    @EventHandler
    public void NewTownEvent(NewTownEvent event) {
        Town town = event.getTown();
        plugin.createChannel(town).thenAccept(role -> {
            Player player = Bukkit.getPlayerExact(town.getMayor().getName());
            if (player == null) return;
            Member member = DiscordSRV.getPlugin().getMainGuild().getMemberById(DiscordSRV.getPlugin().getAccountLinkManager().getDiscordId(player.getUniqueId()));
            if (member == null) return;

            DiscordUtil.addRoleToMember(member, role);
        });
    }

    @EventHandler
    public void DeleteTownEvent(DeleteTownEvent event) {
        String townName = event.getTownName();

        plugin.getOrCreateRole(townName).thenAccept(role -> role.delete().queue());
        List<TextChannel> channels = DiscordSRV.getPlugin().getMainGuild().getCategoryById(plugin.categoryId).getTextChannels();
        for (TextChannel channel : channels) {
            if (channel.getName().equals(plugin.stripTownName(townName))) {
                channel.delete().queue();
                break;
            }
        }
    }

    @EventHandler
    public void TownRenameEvent(RenameTownEvent event) {
        Town town = event.getTown();
        String oldName = event.getOldName();

        plugin.getOrCreateRole(oldName).thenAccept(role -> {
            role.getManager().setName("Town-" + town.getName()).queue();
        });

        List<TextChannel> channels = DiscordSRV.getPlugin().getMainGuild().getCategoryById(plugin.categoryId).getTextChannels();
        for (TextChannel channel : channels) {
            if (channel.getName().equals(plugin.stripTownName(oldName))) {
                channel.getManager().setName(town.getName()).queue();
                break;
            }
        }
    }

    @EventHandler
    public void TownAddResidentEvent(TownAddResidentEvent event) {
       Resident resident = event.getResident();
       Town town = event.getTown();
       Player player = Bukkit.getPlayerExact(resident.getName());
       if (player == null) return;

        TextChannel channel = plugin.getChannel(town);
        if (channel != null) channel.sendMessage(player.getName() + " has joined the town.").queue();

        String id = DiscordSRV.getPlugin().getAccountLinkManager().getDiscordId(player.getUniqueId());
       if (id == null) return;
        Member member = DiscordSRV.getPlugin().getMainGuild().getMemberById(id);
        if (member == null) return;
        if (event.getTown().getMayor() == null) return;

        plugin.getOrCreateRole(town.getName()).thenAccept(role -> DiscordUtil.addRoleToMember(member, role));
    }

    @EventHandler
    public void TownRemoveResidentEvent(TownRemoveResidentEvent event) {
        Resident resident = event.getResident();
        Town town = event.getTown();
        Player player = Bukkit.getPlayerExact(resident.getName());
        if (player == null) return;

        TextChannel channel = plugin.getChannel(town);
        if (channel != null) channel.sendMessage(player.getName() + " has left the town.").queue();

        String id = DiscordSRV.getPlugin().getAccountLinkManager().getDiscordId(player.getUniqueId());
        if (id == null) return;
        Member member = DiscordSRV.getPlugin().getMainGuild().getMemberById(id);
        if (member == null) return;

        plugin.getOrCreateRole(town.getName()).thenAccept(role -> DiscordUtil.removeRolesFromMember(member, role));
    }

    @EventHandler (priority= EventPriority.MONITOR)
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        try {
            Resident resident = TownyAPI.getInstance().getDataSource().getResident(player.getName());
            if (!resident.hasTown()) return;

            Town town = resident.getTown();
            AccountLinkManager manager = DiscordSRV.getPlugin().getAccountLinkManager();
            if (manager == null) return;
            String discordId = manager.getDiscordId(player.getUniqueId());
            if (discordId == null) return;
            Member member = DiscordSRV.getPlugin().getMainGuild().getMemberById(discordId);
            if (member == null) return;

            plugin.getOrCreateRole(town.getName()).thenAccept(role -> DiscordUtil.addRoleToMember(member, role));
        } catch (NotRegisteredException ignored) {}
    }
}
