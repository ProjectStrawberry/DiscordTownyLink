package com.gmail.jd4656.discordtownylink;

import com.palmergames.bukkit.towny.object.Town;
import github.scarsz.discordsrv.DiscordSRV;
import github.scarsz.discordsrv.dependencies.jda.api.Permission;
import github.scarsz.discordsrv.dependencies.jda.api.entities.Category;
import github.scarsz.discordsrv.dependencies.jda.api.entities.Guild;
import github.scarsz.discordsrv.dependencies.jda.api.entities.Role;
import github.scarsz.discordsrv.dependencies.jda.api.entities.TextChannel;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.bukkit.ChatColor.COLOR_CHAR;

public class Main extends JavaPlugin {

    String categoryId = "";

    @Override
    public void onEnable() {
        DiscordSRV.api.subscribe(new DiscordListener(this));
        getServer().getPluginManager().registerEvents(new EventListeners(this), this);
    }

    public String stripTownName(String townName) {
        return townName.replaceAll("[^a-zA-Z\\d\\-_:]", "").toLowerCase();
    }

    public CompletableFuture<Role> getOrCreateRole(String townName) {
        CompletableFuture<Role> completableFuture = new CompletableFuture<>();
        Guild guild = DiscordSRV.getPlugin().getMainGuild();
        List<Role> roles = guild.getRolesByName("Town-" + townName, true);
        if (roles.isEmpty()) {
            guild.createRole().setName("Town-" + townName).queue(completableFuture::complete);
        } else {
            completableFuture.complete(roles.get(0));
        }

        return completableFuture;
    }

    public CompletableFuture<Role> createChannel(Town town) {
        CompletableFuture<Role> completableFuture = new CompletableFuture<>();
        Guild guild = DiscordSRV.getPlugin().getMainGuild();
        Category category = guild.getCategoryById(this.categoryId);
        assert category != null;

        TextChannel townChannel = null;

        for (TextChannel channel : category.getTextChannels()) {
            if (channel.getName().equals(stripTownName(town.getName()))) {
                townChannel = channel;
                break;
            }
        }

        if (townChannel == null) {

        }

        getOrCreateRole(town.getName()).thenAccept(role -> {
            completableFuture.complete(role);
            category.createTextChannel(town.getName()).queue(textChannel -> {
                textChannel.createPermissionOverride(role).setAllow(new Permission[]{Permission.MESSAGE_WRITE, Permission.MESSAGE_READ}).queue();
            });
        });

        return completableFuture;
    }

    public TextChannel getChannel(Town town) {
        Guild guild = DiscordSRV.getPlugin().getMainGuild();
        Category category = guild.getCategoryById(this.categoryId);

        if (category == null || category.getTextChannels().isEmpty()) return null;
        for (TextChannel channel : category.getTextChannels()) {
            if (channel.getName().equals(stripTownName(town.getName()))) {
                return channel;
            }
        }

        return null;
    }

    public static String parseColorCodes(String str) {
        str = ChatColor.translateAlternateColorCodes('&', str);
        str = translateHexColorCodes("#", "", str);

        return str;
    }

    public static String translateHexColorCodes(String startTag, String endTag, String message) {
        final Pattern hexPattern = Pattern.compile(startTag + "([A-Fa-f0-9]{6})" + endTag);
        Matcher matcher = hexPattern.matcher(message);
        StringBuffer buffer = new StringBuffer(message.length() + 4 * 8);
        while (matcher.find()) {
            String group = matcher.group(1);
            matcher.appendReplacement(buffer, COLOR_CHAR + "x"
                    + COLOR_CHAR + group.charAt(0) + COLOR_CHAR + group.charAt(1)
                    + COLOR_CHAR + group.charAt(2) + COLOR_CHAR + group.charAt(3)
                    + COLOR_CHAR + group.charAt(4) + COLOR_CHAR + group.charAt(5)
            );
        }
        return matcher.appendTail(buffer).toString();
    }
}
