package com.gmail.jd4656.discordtownylink;

import com.palmergames.bukkit.towny.TownyAPI;
import com.palmergames.bukkit.towny.exceptions.NotRegisteredException;
import com.palmergames.bukkit.towny.object.Resident;
import com.palmergames.bukkit.towny.object.Town;
import github.scarsz.discordsrv.DiscordSRV;
import github.scarsz.discordsrv.api.Subscribe;
import github.scarsz.discordsrv.api.events.DiscordGuildMessageReceivedEvent;
import github.scarsz.discordsrv.api.events.DiscordReadyEvent;
import github.scarsz.discordsrv.dependencies.jda.api.Permission;
import github.scarsz.discordsrv.dependencies.jda.api.entities.Category;
import github.scarsz.discordsrv.dependencies.jda.api.entities.Guild;
import github.scarsz.discordsrv.dependencies.jda.api.entities.TextChannel;
import github.scarsz.discordsrv.util.PlaceholderUtil;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.*;

public class DiscordListener {
    private final Main plugin;

    DiscordListener(Main p) {
        plugin = p;
    }

    @Subscribe
    public void discordMessage(DiscordGuildMessageReceivedEvent event) {
        Guild guild = DiscordSRV.getPlugin().getMainGuild();

        Category category = guild.getCategoryById(plugin.categoryId);
        if (category == null) return;

        if (category.getTextChannels().contains(event.getChannel())) {
            try {
                Town town = TownyAPI.getInstance().getDataSource().getTown(event.getChannel().getName());

                String username = event.getAuthor().getName();
                String message = event.getMessage().getContentStripped();

                for (Resident resident : town.getResidents()) {
                    Player player = Bukkit.getPlayerExact(resident.getName());
                    if (player == null || !player.isOnline()) continue;

                    player.sendMessage(Main.parseColorCodes("&f[&bDiscord&f] [#ffc13bTC&f] [#798aad" + town.getName() + "&f] " + username + ": #ffc13b" + message));
                }
            } catch (NotRegisteredException ignored) {}
        }
    }

    @Subscribe
    public void discordReadyEvent(DiscordReadyEvent event) {
        Guild guild = DiscordSRV.getPlugin().getMainGuild();
        if (guild.getCategoriesByName("Towns", true).isEmpty()) {
            Collection<Permission> deny = new ArrayList<>();
            deny.add(Permission.MESSAGE_READ);
            deny.add(Permission.MESSAGE_WRITE);
            guild.createCategory("Towns").addPermissionOverride(guild.getPublicRole(), null, deny).queue(category -> {
                plugin.categoryId = category.getId();
                createChannels();
            });
        } else {
            plugin.categoryId = guild.getCategoriesByName("Towns", true).get(0).getId();
            createChannels();
        }
    }

    public void createChannels() {
        List<Town> towns = TownyAPI.getInstance().getDataSource().getTowns();
        Guild guild = DiscordSRV.getPlugin().getMainGuild();
        Category category = guild.getCategoryById(plugin.categoryId);
        assert category != null;
        Map<String, String> channels = new HashMap<>();
        for (TextChannel channel : category.getTextChannels()) {
            channels.put(channel.getId(), channel.getName().toLowerCase());
        }

        for (Town town : towns) {
            if (channels.containsValue(plugin.stripTownName(town.getName()))) continue;
            plugin.createChannel(town);
        }
    }
}
